output_dir = build
source_dir = src

# Put here all your tex source files.
tex_sources = main.tex ack.tex abstract.tex a_chapter.tex

# Put here all your bib source files.
bib_sources = main.bib

sources =  $(patsubst %, $(source_dir)/%, $(tex_sources) $(bib_sources))

# To improve!! This entry rebuild every time... TODO
main.pdf: $(sources)
	mkdir $(output_dir) -p
	TEXINPUTS="$(source_dir):" pdflatex -output-directory $(output_dir) -shell-escape main.tex
	TEXINPUTS="$(source_dir):" pdflatex -output-directory $(output_dir) -shell-escape $(output_dir)/main-frn.tex
	mv $(output_dir)/main-frn.pdf .
	BIBINPUTS="$(source_dir):" BSTINPUT="$(output_dir)" TEXMFOUTPUT="$(output_dir):" bibtex $(output_dir)/main
	TEXINPUTS="$(source_dir):" pdflatex -output-directory $(output_dir) -shell-escape main.tex
	TEXINPUTS="$(source_dir):" pdflatex -output-directory $(output_dir) -shell-escape main.tex
	rm main-frn.pdf

thesis: main.pdf
